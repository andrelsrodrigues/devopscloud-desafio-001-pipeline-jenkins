pipeline {
    agent any

    environment {
		registry = "andrelsrodrigues/estudos-docker-dotnet"
        registryCredential = "dockerhub_id" 
        dockerImage = ''
    }

    stages {
    	stage('Clone Application repository') {
    		steps {  
                git branch: "main", url: 'https://gitlab.com/andrelsrodrigues/estudos-docker-dotnet.git' 
			}
    	}
    	stage('Build Docker Image') {
            steps{
                sh "sudo systemctl start docker"
                script {
                    dockerImage = docker.build(registry + ":DEVELOP", "./samples/aspnetapp") 
                }
            }
        }
    	stage('Send image to Docker Hub') {
            steps{
                script {
                    docker.withRegistry( '', registryCredential) {
                        dockerImage.push()
                    }
                }
            }
        }
    	stage('Clone AWS CodeDeploy scripts repository') {
    		steps {  
                sh "mkdir -p aws-codedeploy-scripts"
                dir('aws-codedeploy-scripts'){
                    git branch: "main", url: 'https://gitlab.com/andrelsrodrigues/devopscloud-desafio-001-aws-codedeploy-scripts.git' 
                }
			}
    	}
    	stage('Release on EC2 instance through CodeDeploy') {
		    steps{
                step([$class: 'AWSCodeDeployPublisher',
                    applicationName: 'estudos-docker-dotnet',
                    awsAccessKey: "AKIAW6PC5RILU45734VI",
                    awsSecretKey: "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",
                    credentials: 'awsAccessKey',
                    deploymentGroupAppspec: false,
                    deploymentGroupName: 'estudos-docker-dotnet-deploymentgroup',
                    deploymentMethod: 'deploy',
                    iamRoleArn: '',
                    subdirectory: 'aws-codedeploy-scripts',
                    includes: '**',
                    excludes: '',
                    pollingFreqSec: 15,
                    pollingTimeoutSec: 600,
                    proxyHost: '',
                    proxyPort: 0,
                    region: 'us-east-2',
                    s3bucket: 'estudos-docker-dotnet-andrelsr',
                    s3prefix: '', 
                    versionFileName: '',
                    waitForCompletion: true])
            }
        }
    	stage('Cleaning up') {
        	steps {
            	sh "docker rmi $registry:DEVELOP"
        	}
		}
    }
}